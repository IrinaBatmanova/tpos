import libtmux
import socket
import os
import tqdm
import argparse
from libtmux import exc

def start(session_name, num_users, base_dir='./'):
    """
    Запустить $num_users ноутбуков. У каждого рабочай директория $base_dir+$folder_num
    """
    server = libtmux.Server()
    session =  server.find_where({"session_name": session_name})
    for i in tqdm.tqdm(range(num_users)):
        w = session.new_window(attach=False, window_name="w" + str(i))
        pane = w.attached_pane
        try:
            os.mkdir(base_dir + str(i))
        except Exception:
            pass
        pane.send_keys("jupyter notebook --ip 127.0.0.1 --port {port} --no-browser " \
                       "--NotebookApp.token='{token}' --NotebookApp.notebook_dir={dir} &\n".format(
                       port=5000 + i, token="a" + str(i), dir=base_dir + str(i)
                       ),
                       enter=False)

def stop(session_name, num):
    """
    @:param session_name: Названия tmux-сессии, в которой запущены окружения
    @:param num: номер окружения, кот. можно убить
    """
    server = libtmux.Server()
    session =  server.find_where({"session_name": session_name})
    window = session.list_windows()[num]
    pane = window.attached_pane
    pane.send_keys("kill -9 $!")


def stop_all(session_name):
    """
    @:param session_name: Названия tmux-сессии, в которой запущены окружения
    """
    server = libtmux.Server()
    session =  server.find_where({"session_name": session_name})
    windows = session.list_windows()
    for w in windows:
        pane = w.attached_pane
        pane.send_keys("kill -9 $!")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run and kill some " \
                                    "jupyter notebooks in tmux.')
    parser.add_argument('--command', '-c',
                        choices=['new', 'run', 'kill',
                        'killall'], required=True,
                        help='Create tmux session, create jupyter sessions, '\
                        'kill jupyter session, kill all jupyter sessions')
    parser.add_argument('--session-name', '-s', required=True,
                        help='Session name')
    parser.add_argument('--num_users', '-n', required=False, type=int,
                        help='Num of jupyter notebooks to run')
    parser.add_argument('--session_id', '-i', required=False, type=int,
                        help='Session id to kill')
    parser.add_argument('--base_dir', '-d', required=False,
                         help='Base dir for jupyter notebooks')
    
    args = parser.parse_args()
    if args.command == 'new':
        os.system("tmux new -s {} -d".format(args.session_name))
    elif args.command == 'run':
        if args.num_users is None:
            parser.error("num_users should be provided with this option")
            exit(1)
        start(args.session_name, args.num_users, args.base_dir if
                args.base_dir is not None else './')
    elif args.command == 'kill':
        if args.session_id is None:
            parser.error("Session id should be provided with this option")
            exit(1)
        stop(args.session_name, args.session_id)
    elif args.command == 'killall':
        stop_all(args.session_name)
